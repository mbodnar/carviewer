package com.mbodnar.carviewer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mbodnar.carviewer.car.CarBrandViewFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            var carBrand = CarBrandViewFragment()
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.act_main_root, carBrand)
                    .commit()
        }
    }
}
