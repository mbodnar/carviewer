package com.mbodnar.carviewer.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by MBodnar on 21.05.2017.
 */

public class RecyclerViewLoader extends LinearLayout {

    private LinearLayout mContainerTop;
    private RecyclerViewCompat mRecyclerView;
    private LinearLayout mContainerBottom;
    private OnLoaderListener mLoaderListener;
    private int mLastCount;
    private boolean mIsFirst = true;

    private RecyclerView.OnScrollListener mScrollDetector = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (mIsFirst) {
                mIsFirst = false;
                return;
            }
            if (!mRecyclerView.canScrollVerticallyCompat(-1)) {
                mLastCount = mRecyclerView.getAdapter().getItemCount();
                if (mLoaderListener != null) {
                    mLoaderListener.startTopLoading();
                }
                topOverhead();
            } else if (!mRecyclerView.canScrollVerticallyCompat(1)) {
                mLastCount = mRecyclerView.getAdapter().getItemCount();
                if (mLoaderListener != null) {
                    mLoaderListener.startBottomLoading();
                }
                bottomOverhead();
            }
        }
    };


    private void bottomOverhead() {
        if (mLoaderListener != null && mLoaderListener.isStartedBottom()) {
            mContainerBottom.setVisibility(VISIBLE);
        }
    }

    private void topOverhead() {
        if (mLoaderListener != null && mLoaderListener.isStartedTop()) {
            mContainerTop.setVisibility(VISIBLE);
        }
    }

    public RecyclerViewLoader(Context context) {
        super(context);
        init();
    }

    public RecyclerViewLoader(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public RecyclerViewLoader(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RecyclerViewLoader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void showHeader(boolean show) {
        mContainerTop.setVisibility(show ? VISIBLE : GONE);
        requestLayout();
    }

    public void showFooter(boolean show) {
        mContainerBottom.setVisibility(show ? VISIBLE : GONE);
        requestLayout();
    }

    public void putHeader(@LayoutRes int resource) {
        View v = LayoutInflater.from(getContext()).inflate(resource, null);
        mContainerTop.addView(v);
    }

    public void putBottom(@LayoutRes int resource) {
        View v = LayoutInflater.from(getContext()).inflate(resource, null);
        mContainerBottom.addView(v);
    }

    public void setOnLoaderListener(OnLoaderListener listener) {
        mLoaderListener = listener;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mRecyclerView.addOnScrollListener(mScrollDetector);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mRecyclerView.removeOnScrollListener(mScrollDetector);
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    private void init() {
        setOrientation(VERTICAL);
        mContainerTop = new LinearLayout(getContext());
        LayoutParams params = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0.15F
        );
        mContainerTop.setLayoutParams(params);
        mContainerTop.setOrientation(VERTICAL);

        mRecyclerView = new RecyclerViewCompat(getContext());
        params = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                1.0F
        );
        mRecyclerView.setLayoutParams(params);
        mRecyclerView.setClickable(true);
        mContainerBottom = new LinearLayout(getContext());
        params = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0.15F
        );
        mContainerBottom.setLayoutParams(params);
        mContainerBottom.setOrientation(VERTICAL);

        mContainerTop.setVisibility(GONE);
        mContainerBottom.setVisibility(GONE);

        addView(mContainerTop);
        addView(mRecyclerView);
        addView(mContainerBottom);

        showHeader(false);
        showFooter(false);

        setClickable(true);
        setFocusable(true);
    }

    public interface OnLoaderListener {

        boolean isStartedTop();

        boolean isStartedBottom();

        void startTopLoading();

        void startBottomLoading();
    }

    public static class RecyclerViewCompat extends RecyclerView {

        public RecyclerViewCompat(Context context) {
            super(context);
        }

        public RecyclerViewCompat(Context context, @Nullable AttributeSet attrs) {
            super(context, attrs);
        }

        public RecyclerViewCompat(Context context, @Nullable AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        public boolean canScrollVerticallyCompat(int direction) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                return canScrollVertically(direction);
            } else {
                return canScrollVerticallyInternal(direction);
            }
        }

        private boolean canScrollVerticallyInternal(int direction) {
            final int offset = computeVerticalScrollOffset();
            final int range = computeVerticalScrollRange() - computeVerticalScrollExtent();
            if (range == 0) return false;
            if (direction < 0) {
                return offset > 0;
            } else {
                return offset < range - 1;
            }
        }
    }
}
