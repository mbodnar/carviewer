package com.mbodnar.carviewer.car.model

import com.viva.somnettelecom.pagination.model.BasePaginationModel
import org.json.JSONObject

class CarListModel: BasePaginationModel<CarModel>() {

    override fun parseData(data: JSONObject) {
        super.parseData(data)
        var arr = data.getJSONObject("wkda")
        arr?.let { parseList(it) }
    }
    private fun parseList(it: JSONObject) {
        var i = 0
        for (key in it.keys()) {
            val item = CarModel()
            item.title = it.getString(key)
            addBottom(item)
        }
    }
}