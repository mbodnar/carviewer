package com.mbodnar.carviewer.car.adapter

import android.content.Context
import android.support.annotation.NonNull
import android.view.LayoutInflater
import android.view.ViewGroup
import com.mbodnar.carviewer.R
import com.mbodnar.carviewer.car.adapter.viewholder.CarBrandViewHolder
import com.mbodnar.carviewer.car.model.CarModel
import com.mbodnar.corelib.toolbar.BaseRecyclerAdapter
import com.mbodnar.corelib.toolbar.OnItemClick

class CarAdapter(context: Context?, listener: OnItemClick) : BaseRecyclerAdapter<CarModel, CarBrandViewHolder>(context, listener) {

    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): CarBrandViewHolder {
        return CarBrandViewHolder(
                LayoutInflater.from(context).inflate(R.layout.item_car_brand, null),
                listener
        )
    }

    override fun onBindViewHolder(holder: CarBrandViewHolder?, position: Int) {
        val  item = getItem(position)
        holder?.mTitle?.text = item.title
    }
}