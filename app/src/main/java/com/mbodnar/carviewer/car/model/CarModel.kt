package com.mbodnar.carviewer.car.model

class CarModel {
    var title: String = ""
    get() = field
    set(value) {
        field = value
    }
}