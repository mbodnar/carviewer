package com.mbodnar.carviewer.car.model

import com.viva.somnettelecom.pagination.model.BasePaginationModel
import org.json.JSONObject

class CarBrandListDataModel : BasePaginationModel<CarBrand>() {

    override fun parseData(data: JSONObject) {
        super.parseData(data)
        var arr = data.getJSONObject("wkda")
        arr?.let {
            parseList(it)
        }
    }

    private fun parseList(it: JSONObject) {
        var i = 0
        for (key in it.keys()) {
            var item = CarBrand()
            item.id = key
            item.title = it.getString(key)
            addBottom(item)
        }
    }
}