package com.mbodnar.carviewer.car.model

class CarBrand {

    private var mTitle: String = ""
    private var mID: String = ""

    var id: String
        get() = mID
        set(value) {
            mID = value
        }
    var title: String
        get() = mTitle
        set(value) {
            mTitle = value
        }
}