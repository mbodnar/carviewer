package com.mbodnar.carviewer.car.loader

import android.content.Context
import android.os.Bundle
import com.mbodnar.carviewer.api.ServerApi
import com.mbodnar.carviewer.car.model.CarListModel
import com.mbodnar.carviewer.core.loader.BaseDataLoader
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call

class CarLoader(context: Context?, args: Bundle?): BaseDataLoader<ResponseBody, CarListModel>(context, args) {

    override fun onSuccess(body: ResponseBody?, model: CarListModel) {
        super.onSuccess(body, model)
        body?.let {
            try {
                var json = JSONObject(body.string())
                model.parseData(json)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
    override fun makeRequest(args: Bundle?): Call<ResponseBody>? {
        return args?.let { ServerApi.instance.api.getCars(it.getInt("page"), PAGE_SIZE, it.getInt("manufacturer")) }
    }


    override fun getModel(): CarListModel = CarListModel()
}