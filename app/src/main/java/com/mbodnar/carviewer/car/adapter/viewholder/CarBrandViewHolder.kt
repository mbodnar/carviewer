package com.mbodnar.carviewer.car.adapter.viewholder

import android.view.View
import android.widget.TextView
import com.mbodnar.carviewer.R
import com.mbodnar.corelib.toolbar.BaseRecyclerAdapter
import com.mbodnar.corelib.toolbar.OnItemClick

class CarBrandViewHolder : BaseRecyclerAdapter.BaseViewHolder {

    val mTitle: TextView

    constructor(itemView: View, listener: OnItemClick): super(itemView, listener) {
        mTitle = itemView.findViewById(R.id.item_car_brand_title)
    }
}