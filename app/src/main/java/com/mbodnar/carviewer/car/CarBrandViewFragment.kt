package com.mbodnar.carviewer.car

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.Loader
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import com.mbodnar.carviewer.R
import com.mbodnar.carviewer.car.adapter.CarBrandAdapter
import com.mbodnar.carviewer.car.loader.CarBrandLoader
import com.mbodnar.carviewer.car.model.CarBrand
import com.mbodnar.carviewer.car.model.CarBrandListDataModel
import com.mbodnar.carviewer.core.pagination.BasePaginationFragment
import com.mbodnar.corelib.ActivityFragmentJongler
import com.viva.somnettelecom.pagination.model.BasePaginationModel

class CarBrandViewFragment : BasePaginationFragment<CarBrandListDataModel>() {

    private lateinit var mAdapter: CarBrandAdapter

    override fun onLoadSuccess(loader: Loader<*>, data: Any) {
        super.onLoadSuccess(loader, data)
        when (loader.id) {
            R.id.loader_load_content -> {
                var list:BasePaginationModel<CarBrand> = getData(data)
                mAdapter.setCollection(list.collection)
                mAdapter.notifyDataSetChanged()
            }
        }
        showContent()
    }

    override fun onLoaderReseted(loader: Loader<*>?) {

    }

    override fun onMenuItemClick(item: MenuItem?): Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mShowBackArrow = false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setTitle("Car Brands")
    }

    override fun createLoader(id: Int, args: Bundle?): Loader<*>? {
        return when (id) {
            R.id.loader_load_content -> args?.let { CarBrandLoader(context, it) }
                    ?: CarBrandLoader(context, null)
            R.id.loader_load_bottom -> args?.let { CarBrandLoader(context, it) }
                    ?: CarBrandLoader(context, null)
            R.id.loader_load_up -> args?.let { CarBrandLoader(context, it) }
                    ?: CarBrandLoader(context, null)
            else -> null
        }
    }

    override fun onClick(viewHolder: RecyclerView.ViewHolder) {
        super.onClick(viewHolder)
        val args = Bundle()
        args.putInt("manufacturer", Integer.parseInt(mAdapter.getItem(viewHolder.adapterPosition).id))
        args.putString("title", mAdapter.getItem(viewHolder.adapterPosition).title)
        val runner = Intent(context, ActivityFragmentJongler::class.java)
        runner.putExtra(ActivityFragmentJongler.BUNDLE_FRAGMENT_CLASS, CarListViewFragment::class.java.name)
        runner.putExtra(ActivityFragmentJongler.BUNDLE_FRAGMENT_ARGUMENTS, args)
        startActivity(runner)

    }

    override val adapter: RecyclerView.Adapter<*>
        get() {
            if (!::mAdapter.isInitialized) {
                mAdapter = CarBrandAdapter(
                        context,
                        this
                )
            }
            return mAdapter;
        }

}