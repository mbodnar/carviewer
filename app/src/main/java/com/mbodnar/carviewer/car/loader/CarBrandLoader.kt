package com.mbodnar.carviewer.car.loader

import android.content.Context
import android.os.Bundle
import com.mbodnar.carviewer.api.ServerApi
import com.mbodnar.carviewer.car.model.CarBrandListDataModel
import com.mbodnar.carviewer.core.loader.BaseDataLoader
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call

class CarBrandLoader(context: Context?, args: Bundle?): BaseDataLoader<ResponseBody, CarBrandListDataModel>(context, args) {

    override fun onSuccess(body: ResponseBody?, model: CarBrandListDataModel) {
        super.onSuccess(body, model)
        body?.let {
            try {
                var json = JSONObject(body.string())
                model.parseData(json)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
    override fun makeRequest(args: Bundle?): Call<ResponseBody> {
        return args?.let { ServerApi.instance.api.getManufacturer(it.getInt("page"), PAGE_SIZE) }
                ?: ServerApi.instance.api.getManufacturer(0, PAGE_SIZE)
    }


    override fun getModel(): CarBrandListDataModel = CarBrandListDataModel()
}