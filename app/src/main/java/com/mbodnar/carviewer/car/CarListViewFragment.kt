package com.mbodnar.carviewer.car

import android.os.Bundle
import android.support.v4.content.Loader
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import com.mbodnar.carviewer.R
import com.mbodnar.carviewer.car.adapter.CarAdapter
import com.mbodnar.carviewer.car.loader.CarLoader
import com.mbodnar.carviewer.car.model.CarModel
import com.mbodnar.carviewer.core.pagination.BasePaginationFragment
import com.viva.somnettelecom.pagination.model.BasePaginationModel

class CarListViewFragment : BasePaginationFragment<CarModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        autoStartLoad = false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setTitle(arguments.getString("title"))
        val args = Bundle()
        args.putInt("manufacturer", arguments.getInt("manufacturer"))
        startLoader(R.id.loader_load_content, args)
    }

    override fun createLoader(id: Int, args: Bundle?): Loader<*>? {
        return when (id) {
            R.id.loader_load_content -> args?.let { CarLoader(context, it) }
                    ?: CarLoader(context, null)
            R.id.loader_load_bottom -> args?.let { CarLoader(context, it) }
                    ?: CarLoader(context, null)
            R.id.loader_load_up -> args?.let { CarLoader(context, it) }
                    ?: CarLoader(context, null)
            else -> null
        }
    }

    override fun onLoadSuccess(loader: Loader<*>, data: Any) {
        super.onLoadSuccess(loader, data)
        when (loader.id) {
            R.id.loader_load_content -> {
                var list: BasePaginationModel<CarModel> = getData(data)
                mAdapter.setCollection(list.collection)
                mAdapter.notifyDataSetChanged()
                if (list.collection.isEmpty()) {
                    showEmptyMessage()
                } else {
                    showContent()
                }
            }
            else -> {
            }
        }
    }

    override fun getEmptyTextMessage(): String {
        return "Cars not found"
    }

    override fun onLoaderReseted(loader: Loader<*>?) {

    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        return false
    }

    private lateinit var mAdapter: CarAdapter
    override val adapter: RecyclerView.Adapter<*>
        get() {
            if (!::mAdapter.isInitialized) {
                mAdapter = CarAdapter(
                        context,
                        this
                )
            }
            return mAdapter
        }
}