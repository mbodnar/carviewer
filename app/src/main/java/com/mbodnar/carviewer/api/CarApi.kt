package com.mbodnar.carviewer.api


import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CarApi {
    @GET("v1/car-types/manufacturer?wa_key=coding-puzzle-client-449cc9d")
    fun getManufacturer(@Query("page") page: Int, @Query("pageSize")pageSize:Int): Call<ResponseBody>

    @GET("v1/car-types/main-types?wa_key=coding-puzzle-client-449cc9d")
    fun getCars(@Query("page") page: Int, @Query("pageSize")pageSize:Int, @Query("manufacturer")manufacturer: Int): Call<ResponseBody>

}