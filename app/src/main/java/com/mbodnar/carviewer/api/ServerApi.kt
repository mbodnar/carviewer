package com.mbodnar.carviewer.api

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.text.DateFormat


class ServerApi {

    private var apiInstance: CarApi

    val api
        get() = apiInstance

    init {
        val retroBuild = Retrofit.Builder()
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        val gson = GsonBuilder()
//                .registerTypeAdapter(Id::class.java, IdTypeAdapter())
//                .enableComplexMapKeySerialization()
                .serializeNulls()
                .setDateFormat(DateFormat.LONG)
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .setVersion(1.0)
                .create()
        retroBuild.client(client)
                .baseUrl("http://api-aws-eu-qa-1.auto1-test.com")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
        apiInstance = retroBuild
                .build()
                .create<CarApi>(CarApi::class.java)
    }

    private object HOLDER {
        val INSTANCE = ServerApi()
    }

    companion object {
        val instance: ServerApi by lazy { HOLDER.INSTANCE }
    }

}