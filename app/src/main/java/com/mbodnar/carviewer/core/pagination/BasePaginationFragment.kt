package com.mbodnar.carviewer.core.pagination

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.Loader
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mbodnar.carviewer.R
import com.mbodnar.carviewer.widget.RecyclerViewLoader
import com.mbodnar.corelib.toolbar.OnItemClick
import com.mbodnar.corelib.toolbar.ToolbarLoaderFragment
import com.mbodnar.corelib.toolbar.loader.network.model.NetworkDataModel
import com.viva.somnettelecom.pagination.model.BasePaginationModel

/**
 * Created by m.bodnar on 28.02.2018.
 */
abstract class BasePaginationFragment<Model> : ToolbarLoaderFragment(), RecyclerViewLoader.OnLoaderListener, OnItemClick {

    protected var recyclerLoader: RecyclerViewLoader? = null
    protected var dataModel: BasePaginationModel<Model>? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var mAdapter: RecyclerView.Adapter<*>? = null

    protected abstract val adapter: RecyclerView.Adapter<*>
    protected var autoStartLoad = true;


    protected fun layoutFragment() = R.layout.frg_base_pagination_dashboard


    override fun onCreateFragmentView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var root = inflater!!.inflate(layoutFragment(), container, false)

        recyclerLoader = root.findViewById(R.id.frg_base_pagination_dashboard_list)

        recyclerLoader?.putHeader(R.layout.loader)
        recyclerLoader?.putBottom(R.layout.loader)
        mLayoutManager = LinearLayoutManager(context)
        savedInstanceState?.let { mLayoutManager?.onRestoreInstanceState(it.getParcelable("scrollState")) }
        mAdapter = adapter
        recyclerLoader?.recyclerView?.layoutManager = mLayoutManager
        recyclerLoader?.recyclerView?.adapter = mAdapter
        recyclerLoader?.setOnLoaderListener(this)

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (autoStartLoad) {
            startLoader(R.id.loader_load_content, null)
        }
    }

    override fun onLoaderDataLoading(id: Int, loader: Loader<*>) {
        when (id) {
            R.id.loader_load_content -> showLoader()
            R.id.loader_load_up -> {
                recyclerLoader?.showHeader(true)
                recyclerLoader?.showFooter(false)
            }
            R.id.loader_load_bottom -> {
                recyclerLoader?.showHeader(false)
                recyclerLoader?.showFooter(true)
            }
        }
    }

    protected open fun onLoadSuccess(loader: Loader<*>, data: Any) {
        when (loader.id) {
            R.id.loader_load_content -> dataModel = getData(data)
            R.id.loader_load_up -> {
                var model : BasePaginationModel<Model> = getData(data)
                var size = model.collection.size
                var oldSize = dataModel!!.collection.size
                dataModel?.addTop(model.collection)
                if (oldSize == dataModel!!.collection.size && oldSize != dataModel?.capacity) {
                    val visible = mLayoutManager!!.findFirstVisibleItemPosition() + size
                    mAdapter!!.notifyDataSetChanged()
                    recyclerLoader!!.getRecyclerView().scrollToPosition(visible)
                }
                dataModel?.topPage = model.topPage
                recyclerLoader!!.showHeader(false)
                recyclerLoader!!.showFooter(false)
                resetLoader(R.id.loader_load_up)
            }
            R.id.loader_load_bottom -> {
                var model : BasePaginationModel<Model> = getData(data)
                var oldSize = dataModel?.collection?.size
                dataModel?.addBottom(model.collection)
                if (oldSize == dataModel?.collection?.size && oldSize != dataModel?.capacity) {
                    var size = model.collection.size
                    mLayoutManager?.let {
                        val visible = it.findFirstVisibleItemPosition() - size
                        dataModel?.collection?.let { mAdapter?.notifyItemRangeChanged(it.size - size, size) }
                        recyclerLoader?.recyclerView?.scrollToPosition(visible)
                    }
                }
                dataModel?.bottomPage = model.bottomPage
                mAdapter?.notifyDataSetChanged()
                recyclerLoader?.showHeader(false)
                recyclerLoader?.showFooter(false)
                resetLoader(R.id.loader_load_bottom)
            }
        }
    }

    protected fun onLoadError(loader: Loader<*>, data: Any) {
        val dataModel : BasePaginationModel<Model> = getData(data)
        when (loader.id) {
            R.id.loader_load_content -> {
                if (dataModel.networkCode == NetworkDataModel.NO_ANY_RESPONSE) {
                    view?.let { Snackbar.make(it, "Huston we got a problem", Snackbar.LENGTH_LONG).show() }
                    showEmptyMessage()
                }
            }
            R.id.loader_load_up -> resetLoader(loader.id)
            R.id.loader_load_bottom -> resetLoader(loader.id)
        }
    }

    override fun onLoadDataFinished(loader: Loader<*>?, data: Any?) {
        if (data is NetworkDataModel) {
            if (data.isSuccess) {
                loader?.let { onLoadSuccess(loader, data) }
            } else {
                loader?.let { onLoadError(loader, data) }
            }
        }
    }

    override fun startTopLoading() {
        if (dataModel != null && onStartTopLoading()) {
            recyclerLoader!!.showHeader(true)
            recyclerLoader!!.showFooter(false)
            val data = Bundle()
            dataModel?.let { data.putInt("page", it.topPage - 1) }
            restartLoader(R.id.loader_load_up, data)
        }
    }

    override fun onClick(viewHolder: RecyclerView.ViewHolder) {

    }

    override fun isStartedTop(): Boolean {
        val loader = loaderManager.getLoader<Any>(R.id.loader_load_up)
        return loader != null && !loader.isReset && loader.isStarted
    }

    override fun isStartedBottom(): Boolean {
        val loader = loaderManager.getLoader<Any>(R.id.loader_load_bottom)
        return loader != null && !loader.isReset && loader.isStarted
    }


    override fun startBottomLoading() {
        if (dataModel != null && onStartBottomLoading()) {
            recyclerLoader?.showHeader(true)
            recyclerLoader?.showFooter(false)
            val data = Bundle()
            dataModel?.let { data.putInt("page", it.bottomPage + 1) }
            restartLoader(R.id.loader_load_bottom, data)
        }
    }

    protected fun onStartBottomLoading(): Boolean {
        return dataModel?.let { it.bottomPage + 1 <= it.totalPage && !isStartedBottom } ?: false
    }

    protected fun onStartTopLoading(): Boolean {
        return dataModel?.let { it.topPage - 1 > 0 && !isStartedBottom } ?: false
    }

    override fun onLongClick(viewHolder: RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable("scrollState",mLayoutManager?.onSaveInstanceState())
    }

    protected fun <T : NetworkDataModel> getData(data: Any): T {
        return data as T
    }
}