package com.mbodnar.carviewer.core.loader

import android.content.Context
import android.os.Bundle
import com.mbodnar.corelib.toolbar.loader.network.NetworkLoader
import com.mbodnar.corelib.toolbar.loader.network.model.NetworkDataModel
import okhttp3.ResponseBody
import org.jetbrains.annotations.Nullable
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

/**
 * Created by m.bodnar on 07.03.2018.
 */

abstract class BaseDataLoader<Resp, Data : NetworkDataModel>(context: Context?, args: Bundle?) : NetworkLoader<Data>(context, args) {

    override fun handleRequest(model: Data, args: Bundle?) {
        val request = makeRequest(args)
        val list = makeRequestList(args)
        try {
            if (list == null) {
                if (request != null) {
                    val response = request.execute()
                    dispatchRequest(response, model)
                }
            } else {
                for (respCall in list) {
                    val response = respCall.query.execute()
                    dispatchRequest(response, model, respCall.id)
                }
            }
        } catch (e: IOException) {
            model.networkCode = NetworkDataModel.NO_ANY_RESPONSE
        } catch (e: Exception) {}
    }

    @Throws(IOException::class)
    private fun dispatchRequest(response: Response<Resp>, model: Data) {
        model.networkCode = response.code()
        if (response.body() != null) {
            onSuccess(response.body(), model)
        }
        if (response.errorBody() != null) {
            onError(response.errorBody()!!, model)
        }
    }

    @Throws(IOException::class)
    private fun dispatchRequest(response: Response<Resp>, model: Data, id: String) {
        model.networkCode = response.code()
        if (response.body() != null) {
            if (response.body()!!.toString().indexOf("\"result\":\"error\"") == -1) {
                onSuccess(response.body(), model, id)
            } else {
                onError(response.body(), model, id)
            }
        }
        if (response.errorBody() != null) {
            onError(response.errorBody()!!, model)
        }
    }

    @Throws(IOException::class)
    private fun onError(responseBody: ResponseBody, model: Data) {

    }

    protected fun onError(body: Resp?, model: Data, id: String) {

    }

    protected open fun onSuccess(body: Resp?, model: Data) {

    }

    protected open fun onSuccess(body: Resp?, model: Data, id: String) {

    }

    @Nullable
    protected abstract fun makeRequest(args: Bundle?): Call<Resp>?
    protected fun makeRequestList(args: Bundle?): List<NetworkQuery<Resp>>? {
        return null
    }

    class NetworkQuery<T>(val id: String, val query: Call<T>)

    companion object {
        val PAGE_SIZE = 25
    }
}
