package com.viva.somnettelecom.pagination.model

import com.google.gson.annotations.SerializedName
import com.mbodnar.corelib.toolbar.loader.network.model.NetworkDataModel
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by m.bodnar on 26.03.2018.
 */

open class BasePaginationModel<Model> : NetworkDataModel() {

    private var mData: MutableList<Model> = ArrayList()

    @SerializedName("pageSize")
    var capacity: Int = 25
        set(value) {
            field = value
            topPage = 0
            bottomPage = 0
        }
    @SerializedName("totalPageCount")
    var totalPage: Int = 0
        get() = field
        set(value) {
            field = value
        }
    var topPage: Int = 0
        get() = field
        set(value) {
            field = value
        }
    var bottomPage: Int = 0
        get() = field
    private var mSize: Int = 0

    val collection: List<Model>
        get() = mData

    val totalRangeObjects: Int
        get() = capacity * 3

    init {
        totalPage = 0
        topPage = 1
        bottomPage = 1
        capacity = 25
    }

    fun addBottom(item: Model) {
        mData.add(item)
        mSize = mData.size
        dispatchTop()
    }

    fun addBottom(item: Collection<Model>) {
        mData.addAll(item)
        mSize = mData.size
        dispatchTop()
    }

    fun addTop(item: List<Model>) {
        val size = item.size
        for (i in size - 1 downTo 0) {
            mData.add(0, item[i])
        }
        mSize = mData.size
        dispatchBottom()
    }

    open fun parseData(data: JSONObject) {
        try {
            capacity = data.getInt("pageSize")
            totalPage = data.getInt("totalPageCount")
            topPage = data.getInt("page")
            bottomPage = topPage
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    private fun dispatchTop() {
        var out = totalRangeObjects - mSize
        if (out < 0) {
            topPage++
            out *= -1
            while (out > 0) {
                mData.removeAt(0)
                out--
            }
        }
    }

    private fun dispatchBottom() {
        var out = totalRangeObjects - mSize
        if (out < 0) {
            topPage--
            bottomPage--
            out *= -1
            var i = mData.size - 1
            while (out > 0) {
                mData.removeAt(i)
                out--
                i--
            }
        }
    }
}
