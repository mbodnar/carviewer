package com.mbodnar.corelib.toolbar.masterdetail;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mbodnar.corelib.ActivityFragmentJongler;
import com.mbodnar.corelib.R;
import com.mbodnar.corelib.toolbar.ToolbarFragment;

import static com.mbodnar.corelib.ActivityFragmentJongler.BUNDLE_FRAGMENT_ARGUMENTS;

/**
 * Created by m.bodnar on 01.02.2018.
 */

public abstract class MasterDetailFragment<Argument extends DetailArgument, Master extends MasterFragment<Argument>, Detail extends DetailFragment<Argument>>
        extends ToolbarFragment
        implements IMaster<Argument>, IDetail<Argument>  {

    public static final int MODE_LAYOUT_MASTER_DETAIL = 1;
    public static final int MODE_LAYOUT_DRAWER = 2;

    public static final String BUNDLE_ARGUMENT = "BUNDLE_ARGUMENT";
    private static final String TAG_FRAGMENT_MASTER = "MasterDetailFragment.MASTER";
    private static final String TAG_FRAGMENT_DETAIL = "MasterDetailFragment.DETAIL";

    private boolean mIsTablet;
    private boolean mRunAsMaster;
    private int mOrientation;
    private Master mMaster;
    private Detail mDetail;
    private Argument mArgument;
    private FragmentManager mFrgManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFrgManager = getChildFragmentManager();
    }

    @Override
    protected final View onCreateFragmentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layout = R.layout.activity_master_detail_acitivity;
        switch (getLayoutMode()) {
            case MODE_LAYOUT_DRAWER:
                layout = R.layout.activity_master_detail_drawer_acitivity;
                break;
        }
        return inflater.inflate(layout, container, false);
    }

    protected boolean isLandscape() {
        return mOrientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    protected boolean isTablet() {
        return mIsTablet;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fetchDeviceSpecific();
        Bundle arg = getArguments();
        mToolbar.setVisibility(View.GONE);

        if (savedInstanceState == null) {
            if (arg != null) {
                mArgument = arg.getParcelable(BUNDLE_ARGUMENT);
            }
        } else {
            mArgument = savedInstanceState.getParcelable(BUNDLE_ARGUMENT);
        }
        mRunAsMaster = mArgument == null;
        if(isAllowTwoFragments()) {
            showListFragment();
            showDetailsFragment(savedInstanceState != null);
        } else if (mRunAsMaster) {
            showListFragment();
        } else {
            showDetailsFragment(savedInstanceState != null);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    private void fetchDeviceSpecific() {
        Configuration config = getResources().getConfiguration();
        mOrientation = getResources().getConfiguration().orientation;
        mIsTablet = (config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    @Override
    public void refreshMaster() {
        if (mMaster != null) {
            mMaster.refreshData();
        }
    }

    @Override
    public boolean onSelectItem(Argument item) {
        boolean hasArgument = mArgument != null;
        mArgument = item;
        return forceSelectItem(hasArgument, false, mArgument);
    }

    @Override
    public void onResetDetail() {
        if (mDetail != null) {
            mDetail.onDataReset();
        }
    }

    @Override
    public void forceAutoSelect() {
        if (mRunAsMaster) {
            mMaster.onAutoSelectItem(mMaster.getAutoSelectedItem());
        }
    }

    public final int getLayoutMode() {
        int mode = MODE_LAYOUT_MASTER_DETAIL;
        if (isTablet()) {
            if (!isLandscape() && useDrawerInPort()) {
                mode = MODE_LAYOUT_DRAWER;
            } else {
                mode = MODE_LAYOUT_MASTER_DETAIL;
            }
        } else {
            if (mArgument != null && useDrawerInPort()) {
                mode = MODE_LAYOUT_DRAWER;
            }
        }
        return mode;
    }

    private boolean forceSelectItem(boolean hasArgument, boolean isAutoSelect, Argument argument) {
        boolean hasSet = false;
        if (isTablet() || !mRunAsMaster) {
            if (hasArgument) {
                mDetail.updateData(argument);
            } else {
                mArgument = argument;
                if (isAutoSelect && isLandscape()) {
                    showDetailsFragment(false);
                    mArgument = null;
                } else if(!isAutoSelect) {
                    showDetailsFragment(false);
                    hasSet = true;
                } else {
                    mArgument = null;
                }
            }
        } else  if (mArgument != null) {
            startDetailFragment();
        }
        return hasSet;
    }

    protected void startDetailFragment() {
        Intent runner = new Intent(getContext(), ActivityFragmentJongler.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_ARGUMENT, mArgument);
        runner.putExtra(BUNDLE_FRAGMENT_ARGUMENTS, bundle);
        prepareDetailArgs(runner);
        startActivity(runner);
        mArgument = null;
    }

    public final boolean isRunAsMaster() {
        return mRunAsMaster;
    }

    public boolean useDrawerInPort() {
        return false;
    }

    @Override
    public boolean onAutoSelect(Argument item) {
        return forceSelectItem(false, true, item);
    }

    @Override
    public void refreshDetail() {
        if(mDetail != null) {
            mDetail.refreshData();
        }
    }

    @Override
    public void resetMaster() {
        if (mMaster != null) {
            mMaster.resetData();
        }
    }

    @Override
    public Argument getDetailArgument() {
        return mArgument;
    }

    public abstract Master createMaster();

    public abstract void prepareMasterArgs(Bundle args);

    public abstract Detail createDetail();

    public void prepareDetailArgs(Bundle args, Argument argument) {

    }

    public abstract void prepareDetailArgs(Intent runner);

    private void showListFragment() {
        mMaster = (Master) mFrgManager.findFragmentByTag(TAG_FRAGMENT_MASTER);
        mDetail = (Detail) mFrgManager.findFragmentByTag(TAG_FRAGMENT_DETAIL);
        if (null == mMaster) {
            Bundle args = new Bundle();
            mMaster = createMaster();
            prepareMasterArgs(args);
            mMaster.setArguments(args);

            mFrgManager.beginTransaction()
                    .replace(R.id.activity_master_detail_master_container, mMaster, TAG_FRAGMENT_MASTER)
                    .commit();
        } else {
            FragmentTransaction transaction = mFrgManager.beginTransaction();
            if (mDetail != null && !mDetail.isDetached()) {
                transaction.detach(mDetail);
            }
            if (mMaster.isDetached()) {
                transaction.attach(mMaster);
            }
            transaction.commit();
        }
    }

    public boolean isAllowTwoFragments() {
        return (isTablet() && isLandscape()) || getLayoutMode() == MODE_LAYOUT_DRAWER;
    }

    private void showDetailsFragment(boolean isRestore) {
        mDetail = (Detail) mFrgManager.findFragmentByTag(TAG_FRAGMENT_DETAIL);
        if (isAllowTwoFragments()) {
            Fragment frg = mFrgManager.findFragmentByTag(TAG_FRAGMENT_DETAIL);
            if (frg != null && isRestore) {
                mFrgManager.beginTransaction().remove(frg).commit();
            }
            if (mDetail == null || !mDetail.isVisible()) {
                mDetail = createDetail();
                Bundle args = new Bundle();
                if (mArgument != null) {
                    prepareDetailArgs(args, mArgument);
                }
                mDetail.setArguments(args);
                mFrgManager.beginTransaction().replace(R.id.activity_master_detail_detail_container, mDetail, TAG_FRAGMENT_DETAIL).commit();
            } else {
                mDetail.initData(mArgument);
            }
        } else if (mArgument != null) {
            Fragment frg = mFrgManager.findFragmentByTag(TAG_FRAGMENT_DETAIL);
            if (frg != null) {
                mFrgManager.beginTransaction().remove(frg).commit();
            }
            Bundle args = new Bundle();
            args.putInt(DetailFragment.BUNDLE_RUN_AS_MASTER, 1);
            mDetail = createDetail();
            if (mArgument != null) {
                prepareDetailArgs(args, mArgument);
            }
            mDetail.setArguments(args);
            FragmentTransaction transaction = mFrgManager.beginTransaction();
            Fragment masterFrg = mFrgManager.findFragmentByTag(TAG_FRAGMENT_MASTER);
            if (masterFrg != null) {
                transaction.detach(mFrgManager.findFragmentByTag(TAG_FRAGMENT_MASTER));
            }
            transaction.add(R.id.activity_master_detail_master_container, mDetail, TAG_FRAGMENT_DETAIL);
            transaction.commit();
        }
    }

    @Override
    public boolean onBackPress() {
        if (getLayoutMode() != MODE_LAYOUT_DRAWER && isTablet() && !isLandscape() && mArgument != null) {
            mArgument = null;
            if (mMaster != null) {
                mMaster.getArguments().putBoolean(MasterFragment.BUNDLE_IS_DETAIL_SHOVE, false);
            }
            showListFragment();
            return false;
        } else {
            return super.onBackPress();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_ARGUMENT, mArgument);
    }
}
