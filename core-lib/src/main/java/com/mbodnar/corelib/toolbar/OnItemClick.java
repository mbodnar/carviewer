package com.mbodnar.corelib.toolbar;

import android.support.v7.widget.RecyclerView;

public interface OnItemClick {
    void onClick(RecyclerView.ViewHolder viewHolder);
    boolean onLongClick(RecyclerView.ViewHolder viewHolder);
}
