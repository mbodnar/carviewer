package com.mbodnar.corelib.toolbar.masterdetail;

/**
 * Created by MBodnar on 11.12.2016.
 */

public interface  IDetail<Arg extends DetailArgument> {
    void refreshMaster();
    void resetMaster();
    void forceAutoSelect();
    Arg getDetailArgument();
}
