package com.mbodnar.corelib;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.mbodnar.corelib.toolbar.ToolbarFragment;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by m.bodnar on 18.01.2018.
 */

public class ActivityFragmentJongler extends AppCompatActivity {

    public static String BUNDLE_FRAGMENT_CLASS = "BUNDLE_FRAGMENT_CLASS";
    public static String BUNDLE_FRAGMENT_ARGUMENTS = "BUNDLE_FRAGMENT_ARGUMENTS";
    private String mFragmentClass;
    private Bundle mArgument;
    private ToolbarFragment mFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_jongler_fragment);

        Bundle extras = getIntent().getExtras();
        Bundle local = null;
        if (savedInstanceState == null) {
            local = extras;
            mFragmentClass = local.getString(BUNDLE_FRAGMENT_CLASS);
            mArgument = local.getBundle(BUNDLE_FRAGMENT_ARGUMENTS);
            if (mArgument == null) {
                mArgument = new Bundle();
            }
            Fragment frg = createInstance(mFragmentClass);
            if (frg != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.act_base_content, frg)
                        .commit();
                frg.setArguments(mArgument);
            } else {
                throw new RuntimeException("Can't create instance of fragment");
            }
            if (frg instanceof ToolbarFragment) {
                mFragment = (ToolbarFragment) frg;
            }
        }
    }

    private Fragment createInstance(String fragmentClass) {
        try {
            Class<?> clazz = Class.forName(fragmentClass);
            Constructor<?> constructor = clazz.getConstructors()[0];
            return (Fragment) constructor.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        if (mFragment != null) {
            if (mFragment.onBackPress()) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_FRAGMENT_CLASS, mFragmentClass);
        outState.putBundle(BUNDLE_FRAGMENT_ARGUMENTS, mArgument);
    }
}
